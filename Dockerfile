FROM tomcat:8.0

MAINTAINER sathishg09

COPY /target/mvn-hello-world.war /usr/local/tomcat/webapps/mvn-hello-world.war

USER root

EXPOSE 8080

WORKDIR /usr/local/tomcat

CMD ["catalina.sh","run"]
